import styled from 'styled-components';

export const Container = styled.section`
  @media screen and (min-width: 768px) {
    padding: 2rem 6rem;
  }

  @media screen and (max-width: 768px) {
    padding: 1rem;
  }
`;
