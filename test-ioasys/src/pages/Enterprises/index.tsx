import React, { useState } from 'react';

import Header from './components/Header';
import Content from './components/Content';
import { Container } from './styles';
import { HeaderContext } from './context/HeaderContext';

const Enterprises: React.FC = () => {
  const [headerTitle, setHeaderTitle] = useState('');

  return (
    <HeaderContext.Provider value={{ headerTitle, setHeaderTitle }}>
      <Header />
      <Container>
        <Content />
      </Container>
    </HeaderContext.Provider>
  );
};

export default Enterprises;
