import React, { useContext, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';

import Image from '../../../../components/Image';
import { useFetchSWR } from '../../../../hooks/swr';
import { toast } from 'react-toastify';
import { IEnterprises } from '../../../../interfaces/Enterprises';
import { HeaderContext } from '../../context/HeaderContext';
import Skeleton from '../../../../components/Skeleton';

import { Container, ImageContainer, Text, SkeltonContainer } from './styles';

const Content: React.FC = () => {
  const { id } = useParams<{ id: string }>();
  const { setHeaderTitle } = useContext(HeaderContext);

  const { data, loading, error } = useFetchSWR<IEnterprises>(
    id && `${process.env.REACT_APP_API_URL}/enterprises/${id}`
  );

  useEffect(() => {
    if (error) {
      toast.error('Houve um erro ao listar a empresa.');
      return;
    }
    if (!data) return;

    setHeaderTitle(data?.enterprise_name);
  }, [error, data, setHeaderTitle]);

  if (loading || !data)
    return (
      <Container>
        <ImageContainer>
          <Skeleton height="100%" width="100%" />
        </ImageContainer>

        <SkeltonContainer>
          {Array(10)
            .fill(10)
            .map(() => (
              <Skeleton key={uuidv4()} height="1.5rem" width="100%" />
            ))}
        </SkeltonContainer>
      </Container>
    );

  return (
    <Container>
      <ImageContainer>
        <Image url={data?.photo} initials={data.enterprise_name} />
      </ImageContainer>

      <Text>{data?.description}</Text>
    </Container>
  );
};

export default Content;
