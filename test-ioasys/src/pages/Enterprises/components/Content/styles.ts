import styled from 'styled-components';

export const Container = styled.div`
  background-color: var(--white-two);
  display: flex;
  flex-direction: column;
  align-items: center;
  min-height: 20rem;
  border-radius: 10px;
  overflow: hidden;
`;

export const ImageContainer = styled.div`
  width: 100%;

  @media screen and (max-width: 580px) {
    height: 10rem;
  }

  @media screen and (min-width: 580px) {
    height: 20rem;
  }
`;

export const Text = styled.p`
  font-family: SourceSansPro-Regular;
  padding: 2.5rem;
  font-size: 2.1rem;
  color: var(--warm-grey);

  @media screen and (max-width: 580px) {
    font-size: 1.5rem;
  }

  @media screen and (min-width: 580px) {
    font-size: 2.1rem;
  }
`;

export const SkeltonContainer = styled.div`
  padding: 2.5rem;
  width: 100%;

  div {
    margin-bottom: 1rem;
  }
`;
