import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  gap: 0.8rem;
`;

export const Title = styled.h1`
  font-family: Roboto;
  color: var(--white-two);
  font-size: 2rem;
`;
