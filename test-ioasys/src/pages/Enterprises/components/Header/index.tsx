import React, { useContext, useMemo } from 'react';

import { BiArrowBack } from 'react-icons/bi';
import { useHistory } from 'react-router-dom';
import { default as HeaderComponent } from '../../../../components/Header';
import { HeaderContext } from '../../context/HeaderContext';

import { Container, Title } from './styles';

const Header: React.FC = () => {
  const { goBack } = useHistory();
  const { headerTitle } = useContext(HeaderContext);

  const headerMemo = useMemo(() => headerTitle, [headerTitle]);

  return (
    <HeaderComponent>
      <Container>
        <BiArrowBack
          onClick={() => goBack()}
          color="var(--white-two)"
          size="2rem"
          style={{ cursor: 'pointer' }}
        />
        <Title>{headerMemo}</Title>
      </Container>
    </HeaderComponent>
  );
};

export default Header;
