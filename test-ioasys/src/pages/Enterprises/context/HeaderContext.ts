import { createContext } from 'react';

interface Context {
  headerTitle: string;
  setHeaderTitle: React.Dispatch<React.SetStateAction<string>>;
}

export const HeaderContext = createContext({} as Context);
