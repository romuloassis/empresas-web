import styled from 'styled-components';

export const Container = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  padding: 5rem;
  width: 100vw;
  height: 100vh;
  overflow: hidden;
`;
