import styled from 'styled-components';

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const ErrorAlert = styled.span`
  animation: moveY 100ms;
  animation-fill-mode: backwards;

  color: var(--error);
  font-size: 0.8rem;
  text-align: center;
  margin-bottom: 1rem;
`;
