import React, { useCallback, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from 'components/Button';
import Input from 'components/Input';
import { HiOutlineMail, HiLockClosed } from 'react-icons/hi';
import * as yup from 'yup';
import { useFetch } from '../../../../hooks/fetch';

import { Form as StyledForm, ErrorAlert } from './styles';
import { IEnterprises } from '../../../../interfaces/Enterprises';
import { useHistory } from 'react-router-dom';

const schema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().required(),
});

const Form: React.FC = () => {
  const { push } = useHistory();
  const [hasError, setHasError] = useState(false);

  const { register, errors, handleSubmit } = useForm({
    resolver: yupResolver(schema),
  });

  const { loading, run } = useFetch<IEnterprises>('users/auth/sign_in', 'POST');

  useEffect(() => window.localStorage.clear(), []);

  useEffect(() => {
    if (!Object.keys(errors).length) return;
    setHasError(true);
  }, [errors, hasError]);

  const onSubmit = useCallback(
    async (payload: { email: string; password: string }) => {
      const { response, error, headers } = await run(payload);

      if (error) {
        setHasError(true);
        return;
      }

      if (!response || !headers) return;

      //LS
      window.localStorage.setItem(
        'auth',
        JSON.stringify({
          'access-token': headers['access-token'],
          client: headers['client'],
          uid: headers['uid'],
        })
      );

      push('/home');
    },
    [run, setHasError, push]
  );

  return (
    <StyledForm onSubmit={(e) => e.preventDefault()}>
      <Input
        name="email"
        ref={register}
        placeholder="E-mail"
        hasError={hasError}
        leftIcon={<HiOutlineMail color="var(--medium-pink)" />}
      />

      <Input
        name="password"
        isPassword
        ref={register}
        hasError={hasError}
        placeholder="Senha"
        leftIcon={<HiLockClosed color="var(--medium-pink)" />}
      />

      {hasError && (
        <ErrorAlert>
          Credenciais informadas são inválidas, tente novamente.
        </ErrorAlert>
      )}

      <Button
        loading={loading}
        label="ENTRAR"
        onClick={handleSubmit(onSubmit)}
      />
    </StyledForm>
  );
};

export default Form;
