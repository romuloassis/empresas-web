import React from 'react';
import LogoImg from '../../../../assets/images/logo-home@3x.png';

import { Container, Logo, Title, SubTitle } from './styles';

const Header: React.FC = () => {
  return (
    <Container>
      <Logo src={LogoImg} alt="logo" />

      <Title>BEM-VINDO AO EMPRESAS</Title>

      <SubTitle className="Text-Style-8">
        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
      </SubTitle>
    </Container>
  );
};

export default Header;
