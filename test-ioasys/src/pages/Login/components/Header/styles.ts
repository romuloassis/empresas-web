import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-bottom: 1.5rem;
`;

export const Logo = styled.img`
  object-fit: contain;
  width: 18.4rem;
  height: 4.5rem;
  margin-bottom: 4.1rem;
`;

export const Title = styled.h1`
  font-family: Roboto-Bold;
  width: 26.1rem;
  height: 1.2rem;
  font-size: 1.5rem;
  text-align: center;
  margin-bottom: 1.5rem;
  color: var(--charcoal-grey);
`;

export const SubTitle = styled.p`
  text-align: center !important;
  width: 20rem;
`;
