import React from 'react';
import Content from './components/Content';
import Header from './components/Header';
import EnterprisesProvider from './context/EnterprisesContext';

import { Container } from './styles';

const Home: React.FC = () => {
  return (
    <EnterprisesProvider>
      <Container>
        <Header />
        <Content />
      </Container>
    </EnterprisesProvider>
  );
};

export default Home;
