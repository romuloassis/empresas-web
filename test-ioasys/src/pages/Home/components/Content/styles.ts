import styled from 'styled-components';

export const MessageContainer = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;

  height: 100%;
`;

export const TextCenterMessage = styled.p`
  font-size: 2rem;
  font-family: Roboto;
  color: var(--charcoal-grey);
`;

export const SkeletonContainer = styled.div`
  display: flex;
  align-items: center;
  gap: 1rem;

  background-color: var(--white-two);
  border-radius: 10px;
  padding: 2rem;

  @media screen and (max-width: 580px) {
    flex-direction: column;
    overflow: hidden;
    margin: 1rem;

    div {
      width: 100%;
    }
  }

  @media screen and (min-width: 580px) {
    padding: 2rem;
    align-items: center;
    margin: 1rem 3rem;

    .first-skeleton {
      width: 20%;
    }
    .second-skeleton {
      width: 80%;
    }
  }
`;
