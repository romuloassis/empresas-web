import styled from 'styled-components';

export const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Title = styled.p`
  font-family: Roboto-Bold;
  font-size: 1.8rem;
  color: var(--dark-indigo);
`;

export const SubTitle = styled.p`
  font-family: Roboto;
  font-size: 1.5rem;
  color: var(--warm-grey);
  font-weight: 500;
`;

export const Label = styled.p`
  font-family: Roboto;
  font-size: 1.1rem;
  color: var(--warm-grey);
`;
