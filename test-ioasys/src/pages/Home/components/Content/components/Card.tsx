import React from 'react';
import { useHistory } from 'react-router-dom';

import { IEnterprises } from '../../../../../interfaces/Enterprises';
import { default as CardComponent } from '../../../../../components/Card';
import { TextContainer, Title, SubTitle, Label } from './styles';

type Props = IEnterprises;

const Card: React.FC<Props> = ({
  enterprise_name,
  country,
  id,
  enterprise_type,
  photo,
}) => {
  const history = useHistory();

  const goToEnterprise = (id: number) => history.push(`/enterprise/${id}`);

  return (
    <CardComponent
      onClick={() => goToEnterprise(id)}
      imageName={enterprise_name}
      image={photo}
      content={
        <TextContainer>
          <Title>{enterprise_name}</Title>
          <SubTitle>{enterprise_type.enterprise_type_name}</SubTitle>
          <Label>{country}</Label>
        </TextContainer>
      }
    />
  );
};

export default Card;
