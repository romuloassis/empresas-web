import React, { useCallback, useContext, useEffect } from 'react';
import { EnterprisesContext } from 'pages/Home/context/EnterprisesContext';
import Card from './components/Card';
import { useFetchSWR } from '../../../../hooks/swr';
import { toast } from 'react-toastify';
import { IEnterprises } from '../../../../interfaces/Enterprises';
import { v4 as uuidv4 } from 'uuid';
import Skeleton from '../../../../components/Skeleton';
import {
  MessageContainer,
  TextCenterMessage,
  SkeletonContainer,
} from './styles';

const Content: React.FC = () => {
  const { state, dispatch, types } = useContext(EnterprisesContext);

  const { data, error, loading } = useFetchSWR<IEnterprises[]>(
    `${process.env.REACT_APP_API_URL}/enterprises`
  );

  const createStore = useCallback(
    (payload: IEnterprises[]) =>
      dispatch({ type: types.CREATE_ENTERPRISE_LIST, payload }),
    [dispatch, types]
  );

  useEffect(() => {
    if (error) {
      toast.error('Houve um erro ao listar as Empresas!');
      return;
    }
    if (!data) return;

    createStore(data);
  }, [data, error, createStore]);

  if (loading)
    return (
      <>
        {Array(4)
          .fill(4)
          .map(() => (
            <SkeletonContainer key={uuidv4()}>
              <div className="first-skeleton">
                <Skeleton width="100%" height="7rem" />
              </div>
              <div className="second-skeleton">
                <Skeleton width="60%" height="1rem" />
                <Skeleton width="45%" height="1rem" />
                <Skeleton width="30%" height="1rem" />
              </div>
            </SkeletonContainer>
          ))}
      </>
    );

  if (!state)
    return (
      <MessageContainer>
        <TextCenterMessage>Clique na busca para iniciar</TextCenterMessage>
      </MessageContainer>
    );

  if (!state.length)
    return (
      <MessageContainer>
        <TextCenterMessage>
          Nenhuma empresa foi encontrada para a busca realizada.
        </TextCenterMessage>
      </MessageContainer>
    );

  return (
    <>
      {state.map((s) => (
        <Card key={uuidv4()} {...s} />
      ))}
    </>
  );
};

export default Content;
