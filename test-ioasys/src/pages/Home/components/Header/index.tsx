import React, { useCallback, useState } from 'react';

import { HiSearch } from 'react-icons/hi';
import LogoImg from '../../../../assets/images/logo-nav.png';
import SearchBar from './components/SearchBar';
import { default as HeaderComponent } from '../../../../components/Header';
import { Image } from './styles';

const Header: React.FC = () => {
  const [showSearchBar, setShowSearchBar] = useState(false);

  const handleToggleSearchBar = useCallback(() => setShowSearchBar((s) => !s), [
    setShowSearchBar,
  ]);

  if (showSearchBar)
    return (
      <HeaderComponent>
        <SearchBar handleToggleSearchBar={handleToggleSearchBar} />
      </HeaderComponent>
    );

  return (
    <HeaderComponent>
      <Image src={LogoImg} alt="logo-header" />
      <HiSearch
        onClick={handleToggleSearchBar}
        color="var(--white-two)"
        size="2rem"
        style={{ cursor: 'pointer' }}
      />
    </HeaderComponent>
  );
};

export default Header;
