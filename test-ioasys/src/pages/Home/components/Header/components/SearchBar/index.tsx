import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import { HiSearch, HiX } from 'react-icons/hi';

import Input from '../../../../../../components/Input';
import { useFetch } from '../../../../../../hooks/fetch';
import { useDebounce } from '../../../../../../hooks/debounce';
import { Container, ButtonHiddenSearchBar } from './styles';
import { EnterprisesContext } from '../../../../context/EnterprisesContext';
import { IEnterprises } from '../../../../../../interfaces/Enterprises';

interface Props {
  handleToggleSearchBar: () => void;
}

const SearchBar: React.FC<Props> = ({ handleToggleSearchBar }) => {
  const { dispatch, types } = useContext(EnterprisesContext);
  const ref = useRef({ inputValue: '', onChange: false });

  const [value, setValue] = useState('');
  const debounceValue = useDebounce(value, 500);

  const { run } = useFetch<IEnterprises[]>(
    `${process.env.REACT_APP_API_URL}/enterprises?name=${debounceValue}`,
    'GET'
  );

  useEffect(() => {
    if (!ref.current.onChange) return;

    const load = async () => {
      ref.current.onChange = false;
      const { response, error } = await run({});

      if (error || !response) {
        dispatch({ type: types.CLEAR });
        return;
      }

      dispatch({ type: types.CREATE_ENTERPRISE_LIST, payload: response });
    };

    load();
  }, [debounceValue, ref, run]);

  const handleInputChange = useCallback(
    ({ currentTarget }: React.ChangeEvent<HTMLInputElement>) => {
      setValue(() => currentTarget?.value);
      ref.current.onChange = true;
    },
    [ref, setValue]
  );

  return (
    <Container>
      <Input
        placeholder="Pesquisar"
        leftIcon={<HiSearch size="1.5rem" color="var(--white-two)" />}
        rightIcon={
          <ButtonHiddenSearchBar onClick={handleToggleSearchBar}>
            <HiX size="1rem" color="var(--white-two)" />
          </ButtonHiddenSearchBar>
        }
        w="100%"
        h="3rem"
        value={value}
        onChange={handleInputChange}
      />
    </Container>
  );
};

export default SearchBar;
