import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;

  .forward-input {
    border-color: var(--white-two);
    caret-color: var(--white-two);
    color: #991237;
    font-size: 1.5rem;
    font-family: Roboto-Bold;

    &::placeholder {
      color: #991237;
    }
  }
`;

export const ButtonHiddenSearchBar = styled.button`
  background-color: transparent;
  border: transparent;
  cursor: pointer;
  z-index: 1;
`;
