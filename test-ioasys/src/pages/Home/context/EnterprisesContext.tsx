import React, { createContext, Dispatch, useReducer } from 'react';

import { IEnterprises } from 'interfaces/Enterprises';
import {
  types,
  reducer,
  EnterprisesActions,
} from '../reducers/EnterprisesReducer';

interface Context {
  state: IEnterprises[] | undefined;
  dispatch: Dispatch<EnterprisesActions>;
  types: typeof types;
}

export const EnterprisesContext = createContext({} as Context);

const EnterprisesProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, undefined);

  return (
    <EnterprisesContext.Provider value={{ state, dispatch, types }}>
      {children}
    </EnterprisesContext.Provider>
  );
};

export default EnterprisesProvider;
