import { IEnterprises } from 'interfaces/Enterprises';

export enum types {
  CREATE_ENTERPRISE_LIST = 'CREATE_ENTERPRISE_LIST',
  CLEAR = 'CLEAR',
}

export type EnterprisesActions =
  | {
      type: types.CREATE_ENTERPRISE_LIST;
      payload: IEnterprises[];
    }
  | { type: types.CLEAR };

export const reducer = (
  state: IEnterprises[] | undefined,
  action: EnterprisesActions
) => {
  switch (action.type) {
    case types.CREATE_ENTERPRISE_LIST:
      return [...action.payload];

    case types.CLEAR:
      return undefined;

    default:
      return undefined;
  }
};
