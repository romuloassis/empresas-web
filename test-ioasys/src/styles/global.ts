import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
* {
    margin: 0;
    padding:0;
    outline:0;
    box-sizing:border-box;
  }

  :root{    
   
    --white: #d8d8d8;
    --white-two: #ffffff;
    --dark-indigo: #1a0e49;
    --warm-grey: #8d8c8c;
    --greeny-blue: #57bbbc;
    --medium-pink: #ee4c77;
    --night-blue: #0d0430;
    --charcoal-grey: #383743;
    --charcoal-grey-two: #403e4d;
    --greyish: #b5b4b4;
    --error: #ff0f44;
    background-color: #eeecdb;
    font-size:16px;
  }

  @font-face {
  font-family: Roboto;
  src: local(Roboto-Regular),
    url(../assets/fonts/Roboto-Regular.ttf) format("ttf");
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
}

@font-face {
  font-family: Roboto;
  src: local(Roboto-Bold),
    url(../assets/fonts/Roboto-Bold.ttf) format("ttf");
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
}

@font-face {
  font-family: SourceSansPro;
  src: local(SourceSansPro-Regular),
    url(../assets/fonts/SourceSansPro-Regular.ttf) format("ttf");;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
}

.Text-Style-8 {
  font-family: Roboto;
  font-size: 1.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: var(--warm-grey);
}

@keyframes moveY {
    from {
      opacity: 0;
      transform: translateY(-20%);
    }
    to {
      opacity: 1;
      transform: translateY(-0%);
    }
  }

@keyframes moveX {
    from {
      opacity: 0;
      transform: translateX(-20%);
    }
    to {
      opacity: 1;
      transform: translateX(-0%);
    }
  }
`;
