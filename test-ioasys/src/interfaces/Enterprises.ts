export interface IEnterprises {
  readonly id: number;
  readonly description: string;
  readonly enterprise_name: string;
  readonly city: string;
  readonly country: string;
  readonly enterprise_type: {
    id: number;
    enterprise_type_name: string;
  };
  readonly email_enterprise?: null;
  readonly facebook?: null;
  readonly twitter?: null;
  readonly linkedin?: null;
  readonly photo?: string;
}
