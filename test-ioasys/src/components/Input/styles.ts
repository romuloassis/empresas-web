import styled from 'styled-components';
import { RiErrorWarningFill } from 'react-icons/ri';

export const Container = styled.div<{
  w?: string;
  h?: string;
  hasError: boolean;
  leftIconSize?: string;
}>`
  width: ${({ w }) => (!!w ? w : '22rem')};
  height: ${({ h }) => (!!h ? h : '2rem')};

  position: relative;

  margin-bottom: 2rem;
  padding: 1rem;

  .forward-input {
    height: calc(${({ h }) => (!!h ? h : '2rem')} - 0.5rem);

    position: relative;
    width: 100%;
    border-top-width: 0;
    border-left-width: 0;
    border-right-width: 0;
    border-bottom-width: 0.7px;
    border-bottom-color: ${({ hasError }) =>
      hasError ? 'var(--error)' : 'var(--charcoal-grey)'};
    background-color: transparent;
    padding-left: calc(
      ${({ leftIconSize }) => leftIconSize ?? '1rem'} + 0.5rem
    );

    opacity: 0.5;
    font-size: 1.12rem;
    color: var(--charcoal-grey);
  }
`;

export const IconError = styled(RiErrorWarningFill)`
  animation: moveY 450ms;
  animation-delay: 200ms;
  animation-fill-mode: backwards;
`;
