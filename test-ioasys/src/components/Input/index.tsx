import React, { useCallback, useMemo, useState } from 'react';

import { Container, IconError } from './styles';
import { HiEye, HiEyeOff } from 'react-icons/hi';

interface Props
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  leftIcon: JSX.Element;
  rightIcon?: JSX.Element;
  w?: string;
  h?: string;
  hasError?: boolean;
  isPassword?: boolean;
  style?: React.CSSProperties;
}

const Input: React.ForwardRefRenderFunction<HTMLInputElement, Props> = (
  {
    w,
    h,
    leftIcon,
    hasError = false,
    isPassword = false,
    rightIcon,
    style,
    ...props
  },
  ref
) => {
  const [type, setType] = useState(isPassword ? 'password' : props.type);

  const LeftIcon = useMemo(
    () =>
      leftIcon &&
      React.cloneElement(leftIcon, {
        size: leftIcon.props.size ?? '1rem',
        style: { position: 'absolute', bottom: 0 },
        className: 'left-icon',
        ...leftIcon.props,
      }),
    [leftIcon]
  );

  const handleShowOrHidePassword = useCallback(
    () => setType((type) => (type === 'password' ? 'text' : 'password')),
    [setType]
  );

  const RightIcon = useMemo(() => {
    const style: React.CSSProperties = {
      position: 'absolute',
      bottom: 0,
      right: '1.5rem',
      zIndex: 1,
      cursor: 'pointer',
    };

    if (hasError)
      return <IconError size="1.5rem" color="var(--error)" style={style} />;

    if (rightIcon)
      return React.cloneElement(rightIcon, {
        size: rightIcon?.props?.size ?? '1.5rem',
        style,
        ...rightIcon.props,
      });

    if (isPassword) {
      return React.createElement(type === 'text' ? HiEyeOff : HiEye, {
        onClick: () => handleShowOrHidePassword(),
        size: '1.5rem',
        color: 'rgba(0, 0, 0, 0.54)',
        style,
      });
    }

    return null;
  }, [type, isPassword, hasError, handleShowOrHidePassword, rightIcon]);

  return (
    <Container
      w={w}
      h={h}
      hasError={hasError}
      leftIconSize={LeftIcon?.props?.size}
    >
      {LeftIcon}
      {RightIcon}
      <input
        type={type}
        className="forward-input"
        ref={ref}
        style={style}
        {...props}
      />
    </Container>
  );
};

export default React.forwardRef(Input);
