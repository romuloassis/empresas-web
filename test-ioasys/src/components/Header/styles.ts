import styled from 'styled-components';

export const Container = styled.header`
  background-image: linear-gradient(
    178deg,
    var(--medium-pink) 13%,
    var(--night-blue) 180%
  );

  padding: 1rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 4.5rem;
`;
