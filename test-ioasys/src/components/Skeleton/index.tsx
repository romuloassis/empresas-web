import React from 'react';

import { Container } from './styles';

interface ISkeleton {
  width: string;
  height: string;
}

const Skeleton: React.FC<ISkeleton> = ({ width, height }) => (
  <Container width={width} height={height} />
);

export default Skeleton;
