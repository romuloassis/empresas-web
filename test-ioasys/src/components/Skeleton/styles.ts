import React from 'react';
import styled, { css } from 'styled-components';

export const Container = styled(({ height, width, animated, ...rest }) =>
  React.createElement('div', { ...rest }, rest.children)
)`
  border-radius: 3px;
  margin-bottom: 7px;
  width: ${(props: { width: string }) => props.width};
  height: ${(props: { height: string }) => props.height};

  ${(props: { width: string; avatar?: boolean }) =>
    props.avatar &&
    css`
      border-radius: calc(${props.width} / 2);
    `};

  background-color: #666;
  background: linear-gradient(-90deg, #f3f3f3 0%, #dddddd 50%, #f5f5f5 100%);
  background-size: 400% 400%;
  animation: pulse 1.2s ease-in-out infinite;
  @keyframes pulse {
    0% {
      background-position: 0% 0%;
    }
    100% {
      background-position: -135% 0%;
    }
  }
`;
