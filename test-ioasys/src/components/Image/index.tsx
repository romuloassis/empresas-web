import React from 'react';

import { Img, FakeImg } from './styles';

interface Props {
  initials: string;
  url?: string;
}

const Image: React.FC<Props> = ({ url, initials }) => {
  const name = initials.slice(0, 2);

  if (!url)
    return (
      <FakeImg>
        <span>{name}</span>
      </FakeImg>
    );

  return <Img src={`${process.env.REACT_APP_API_URL + url}`} alt={initials} />;
};

export default Image;
