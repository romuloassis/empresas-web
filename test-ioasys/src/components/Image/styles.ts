import styled from 'styled-components';

export const Img = styled.img`
  width: 100%;
  height: 100%;

  &:hover {
    filter: grayscale(20%);
  }
`;

export const FakeImg = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: var(--greeny-blue);

  span {
    color: var(--white-two);
    font-size: 4rem;
    font-family: Roboto-Bold;
    text-transform: uppercase;
  }
`;
