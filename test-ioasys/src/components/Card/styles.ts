import styled from 'styled-components';

export const Container = styled.div`
  display: flex;

  @media screen and (max-width: 580px) {
    flex-direction: column;
    overflow: hidden;
    margin: 1rem;
  }

  @media screen and (min-width: 580px) {
    padding: 2rem;
    align-items: center;
    margin: 1rem 3rem;
  }

  background-color: var(--white-two);
  border-radius: 10px;
`;

export const Content = styled.div`
  padding: 1rem;
`;

export const ImageContainer = styled.div`
  height: 8rem;

  @media screen and (min-width: 580px) {
    width: 15rem;
    * {
      border-radius: 0.5rem;
    }
  }

  @media screen and (max-width: 580px) {
    width: 100%;
  }
`;
