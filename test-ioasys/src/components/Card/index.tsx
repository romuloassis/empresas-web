import React from 'react';

import Image from '../../components/Image';
import { Container, Content, ImageContainer } from './styles';

interface Props {
  content: JSX.Element;
  imageName: string;
  image?: string;
  onClick?: () => void;
}

const Card: React.FC<Props> = ({ image, content, imageName, ...props }) => {
  return (
    <Container {...props}>
      <ImageContainer>
        <Image url={image} initials={imageName} />
      </ImageContainer>
      <Content>{content}</Content>
    </Container>
  );
};

export default Card;
