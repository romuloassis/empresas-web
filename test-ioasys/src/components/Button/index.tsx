import React from 'react';
import { Container } from './styles';

interface Props extends React.HTMLAttributes<HTMLButtonElement> {
  label: string;
  loading?: boolean;
  isBlocked?: boolean;
}

const Button: React.FC<Props> = ({
  loading = false,
  isBlocked = false,
  label,
  ...rest
}) => (
  <Container
    loading={loading}
    isBlocked={isBlocked}
    {...(isBlocked && { disabled: true })}
    {...rest}
  >
    {loading ? 'Processando...' : label}
  </Container>
);

export default Button;
