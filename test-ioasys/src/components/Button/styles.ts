import styled, { css } from 'styled-components';
import { darken } from 'polished';

export const Container = styled.button<{
  isBlocked: boolean;
  loading: boolean;
}>`
  ${({ isBlocked }) =>
    !isBlocked
      ? css`
          background-color: var(--greeny-blue);
        `
      : css`
          background-color: #748383;
          opacity: 0.56;
        `}

  ${({ loading }) =>
    !!loading &&
    `
      background: linear-gradient(
        -90deg,
        rgb(87, 187, 188) 0%,
        ${darken(0.15, 'rgb(87, 187, 188)')} 50%,
        ${darken(0.05, 'rgb(87, 187, 188)')} 100%
      );
      background-size: 400% 400%;
      animation: pulse 1.2s ease-in-out infinite;
      @keyframes pulse {
        0% {
          background-position: 0% 0%;
        }
        100% {
          background-position: -135% 0%;
        }
      }
    `}

  width: 20.25rem;
  height: 3.3rem;
  font-size: 1.25rem;
  border-width: 0;
  border-radius: 4px;
  font-family: GillSans;
  text-align: center;
  color: var(--white-two);
  cursor: pointer;

  &:hover {
    ${({ isBlocked }) =>
      !isBlocked &&
      css`
        background-color: ${darken(0.05, 'rgb(87, 187, 188)')};
      `};
  }
`;
