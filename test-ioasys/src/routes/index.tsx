import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Login from '../pages/Login';
import Home from '../pages/Home';
import Enterprises from '../pages/Enterprises';
import RouteWrapper from './RouteWrapper';

const Routes: React.FC = () => {
  return (
    <Switch>
      <RouteWrapper path="/" exact component={Login} />
      <RouteWrapper path="/home" exact isPrivate component={Home} />
      <RouteWrapper
        path="/enterprise/:id"
        exact
        isPrivate
        component={Enterprises}
      />

      <Route path="*" component={() => <Redirect to="/" />} />
    </Switch>
  );
};

export default Routes;
