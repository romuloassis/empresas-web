import React from 'react';
import { Route, Redirect } from 'react-router-dom';

interface Props {
  component: React.PropsWithChildren<any>;
  isPrivate?: boolean;
  path: string;
  exact?: boolean;
}

const RouteWrapper: React.FC<Props> = ({
  component: Component,
  isPrivate = false,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        isPrivate && !window.localStorage.getItem('auth') ? (
          <Redirect to="/" />
        ) : (
          <Component {...props} />
        )
      }
    />
  );
};

export default RouteWrapper;
