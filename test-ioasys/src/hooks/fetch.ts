import { useState, useCallback, useRef } from 'react';
import { Method, AxiosError } from 'axios';

import api from '../service/api';

export const useFetch = <T = any>(url: string, method: Method) => {
  const [loading, setLoading] = useState(false);

  const ref = useRef({
    response: {} as T,
    error: undefined,
    headers: {} as any,
  });

  const run = useCallback(
    async (data: any) => {
      const axiosRequest = async () => {
        try {
          setLoading(true);

          const response = await api({ method, url, data });

          ref.current.response = response.data;
          ref.current.headers = response.headers;
          ref.current.error = undefined;
        } catch (e) {
          ref.current.response = {} as T;
          ref.current.headers = {};
          ref.current.error = e;

          if (e.response)
            ((ref.current.error as unknown) as AxiosError) = e.response;
        }
      };

      await axiosRequest();
      setLoading(false);

      return { ...ref.current, loading };
    },
    [api, url, method, ref, loading]
  );

  return { loading, run };
};
