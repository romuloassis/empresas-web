import { useState } from 'react';
import useSWR, {
  mutate as mutateGlobal,
  keyInterface,
  ConfigInterface,
} from 'swr';

import api from '../service/api';

export const useFetchSWR = <T = any, E = any>(
  url: keyInterface,
  configs?: ConfigInterface<T, E>
) => {
  const [loading, setLoading] = useState(false);

  const { data, error, mutate } = useSWR<T, E>(
    url,
    async (url) => {
      setLoading(true);
      return api
        .get(url)
        .then((response) => response.data)
        .finally(() => setLoading(false));
    },
    { ...configs }
  );

  return { loading, data, error, mutate, mutateGlobal };
};
