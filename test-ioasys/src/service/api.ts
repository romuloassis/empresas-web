import axios from 'axios';

const defaultOptions = {
  baseURL: `${process.env.REACT_APP_API_URL}/api/v1`,
  headers: {
    'Content-Type': 'application/json',
  },
};

// Create instance
const api = axios.create(defaultOptions);

interface Headers {
  accessToken?: string;
  client?: string;
  uid?: string;
}

// Set the AUTH token for any request
api.interceptors.request.use((config) => {
  const auth = window.localStorage.getItem('auth');
  if (auth) {
    const headers = JSON.parse(auth);
    const { client, uid }: Headers = headers;

    config.headers = {
      'Access-Control-Allow-Origin': '*',
    };

    config.headers['access-token'] = headers['access-token'] ?? '';
    config.headers.client = client ?? '';
    config.headers.uid = uid ?? '';
  }

  return config;
});

// Verify if user is authenticated
api.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error?.response?.status === 401) {
      window.localStorage.clear();
      return;
    }

    return Promise.reject(error);
  }
);

export default api;
